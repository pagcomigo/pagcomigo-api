const requestGateway = require('../helpers/requestGateway');
const { customers, transactions, sequelize } = require('../models');
const errorHandler = require('../helpers/error');

module.exports.create = async (body) => {
  try {
    const customerInstance = customers.build(body.customer);
    const transactionInstance = transactions.build(body.transaction);

    await Promise.all([
      customerInstance.validate(),
      transactionInstance.validate({ skip: ['customer_id', 'transaction_gateway_id'] })
    ])

    await requestGateway(transactionInstance);

    await sequelize.transaction((t) => {
      return customers.upsert(body.customer, { transaction: t }).then(upsertResult => {
        return customers.findOne({
          where: {
            email: customerInstance.email
          },
          attributes: ['id']
        }, { transaction: t }).then(result => {
          transactionInstance.customer_id = result.id;
          return transactionInstance.save({ transaction: t });
        })
      });
    })

    return {
      body: transactionInstance,
      statusCode: 201
    }

  } catch(err) {
    return errorHandler(err);
  }
}