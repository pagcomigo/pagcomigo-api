const axios = require('axios');
const { convertObjectToGateway, handleResponse } = require(`./${process.env.GATEWAY}`);

const axiosInstance = axios.create({
  baseURL: process.env.GATEWAY_URL,
  timeout: 8000,
  headers: {
    'Content-Type': 'application/json'
  }
})

if(process.env.GATEWAY === 'cielo') {
  axiosInstance.defaults.headers['MerchantId'] = process.env.CIELO_MERCHANT_ID;
  axiosInstance.defaults.headers['MerchantKey'] = process.env.CIELO_MERCHANT_KEY;
} else {
  axiosInstance.defaults.headers['authorization'] = `Basic ${process.env.REDE_BASIC_AUTH}`;
}

const sendTransactionToGateway = async function(transaction) {
  const payload = convertObjectToGateway(transaction);

  console.log(payload);

  const response = await axiosInstance.post(
    process.env.GATEWAY === 'cielo' ? '/1/sales' : '/v1/transactions' ,
    payload
  );

  handleResponse(response, transaction);

  return response.data;
}

module.exports = sendTransactionToGateway