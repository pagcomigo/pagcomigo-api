const uuid = require('uuid');

const convertObjectToGateway = (transaction) => {
  const cieloTransaction = {
    MerchantOrderId: uuid.v4(),
    Payment: {
      Type: 'CreditCard',
      Amount: transaction.amount * 100,
      Installments: transaction.installments,
      Capture: true,
      SoftDescriptor: 'Pagcomigo', //Vir do environtment
      CreditCard: {
        CardNumber: transaction.creditCard.number,
        Holder: transaction.creditCard.holder,
        ExpirationDate: transaction.creditCard.expiration,
        SecurityCode: transaction.creditCard.cvv,
        Brand: transaction.creditCard.brand,
      }
    }
  }

  return cieloTransaction;
}

const handleResponse = function(response, transaction) {
  if (response.data.Payment.Status === 2) {
    transaction.transaction_gateway_id = response.data.Payment.Tid;
    return true;
  }
  throw Error(response.data.Payment.ReturnMessage);
}

module.exports = {
  convertObjectToGateway,
  handleResponse
}