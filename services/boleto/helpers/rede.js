const convertObjectToGateway = function(transaction) {
  const redeTransaction = {
    reference: new Date().getTime(),
    Amount: transaction.amount * 100,
    installments: transaction.installments,
    cardHolderName: transaction.creditCard.holder,
    cardNumber: transaction.creditCard.number,
    expirationMonth: Number(transaction.creditCard.expiration.split('/')[0]),
    expirationYear: Number(transaction.creditCard.expiration.split('/')[1]),
    securityCode: transaction.creditCard.cvv,
    softDescriptor: 'Pagcomigo'
  }

  return redeTransaction;
}

const handleResponse = function(response, transaction) {
  if (response.data.returnCode === '00') {
    transaction.transaction_gateway_id = response.tid;
    return true;
  }
  else {
    throw Error(response.data.returnMessage);
  }
}

module.exports = {
  convertObjectToGateway,
  handleResponse
}