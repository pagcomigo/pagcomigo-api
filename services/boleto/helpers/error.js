function filterError(err) {
  if (err.name === 'SequelizeUniqueConstraintError' || err.name === 'SequelizeValidationError') {
    console.log(err);
    return {
      body: err.errors,
      statusCode: 400
    }
  }

  return {
    body: err.message,
    statusCode: 500
  }
}

module.exports = filterError;