const Joi = require('@hapi/joi');

const creditCardSchema = Joi.object({
  number: Joi.string().creditCard().required(),
  holder: Joi.string().required(),
  expiration: Joi.string().required(),
  cvv: Joi.string().max(4).required(),
  brand: Joi.string().valid(
    'Visa',
    'Master',
    'Amex',
    'Elo',
    'Aura',
    'JCB',
    'Diners',
    'Discover',
    'Hipercard',
    'Hiper',
  ).required()
})

module.exports = {
  creditCardSchema
};