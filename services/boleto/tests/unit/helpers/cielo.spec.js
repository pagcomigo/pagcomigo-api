const { convertObjectToGateway, handleResponse } = require('../../../helpers/cielo');

describe('Cielo unit testing', () => {
  test('Testing converting internal transaction to cielo', () => {
    const input = {
      "boleto": "123",
      "paymentMethod": "CreditCard",
      "amount": 10.46,
      "installments": 1,
      "creditCard": {
        "number": "5203510265142101",
        "holder": "Fernando A Z Penna",
        "expiration": "12/2020",
        "cvv": "123",
        "brand": "Master"
      }
    };

    const result = convertObjectToGateway(input);

    const expected = {
      MerchantOrderId: result.MerchantOrderId,
      Payment: {
        Type: 'CreditCard',
        Amount: 1046,
        Installments: 1,
        Capture: true,
        SoftDescriptor: 'Pagcomigo',
        CreditCard: {
          CardNumber: "5203510265142101",
          Holder: "Fernando A Z Penna",
          ExpirationDate: "12/2020",
          SecurityCode: "123",
          Brand: "Master",
        }
      }
    }

    expect(result).toStrictEqual(expected);
  })
})