const { convertObjectToGateway, handleResponse } = require('../../../helpers/rede');

describe('Rede unit testing', () => {
  test('Testing converting internal transaction to rede', () => {
    const input = {
      "boleto": "123",
      "paymentMethod": "CreditCard",
      "amount": 10.46,
      "installments": 1,
      "creditCard": {
        "number": "5203510265142101",
        "holder": "Fernando A Z Penna",
        "expiration": "12/2020",
        "cvv": "123",
        "brand": "Master"
      }
    };

    const result = convertObjectToGateway(input);

    const expected = {
      reference: result.reference,
      Amount: 1046,
      installments: 1,
      cardHolderName: "Fernando A Z Penna",
      cardNumber: "5203510265142101",
      expirationMonth: 12,
      expirationYear: 2020,
      securityCode: "123",
      softDescriptor: 'Pagcomigo'
    }

    expect(result).toStrictEqual(expected);
  })

  test('Testing handling response error', () => {
    const response = {
      reference: '1582757687513',
      tid: '10012002261937070004',
      nsu: '237129780',
      authorizationCode: '119615',
      brandTid: '119615',
      dateTime: '2020-02-26T18:27:41-03:00',
      amount: 1046,
      cardBin: '520247',
      last4: '0912',
      returnCode: '58',
      returnMessage: 'Unauthorized. Contact issuer.'
    }

    expect(() => handleResponse(input)).toThrow(Error);
  })

  test('Testing handling response success', () => {
    const response = {
      data: {
        reference: "pedido123",
        tid: "8345000363484052380",
        nsu: "663206341",
        authorizationCode: "186376",
        dateTime: "2017-02-28T08:54:00.000-03:00",
        amount: 2099,
        cardBin: "544828",
        last4: "0007",
        returnCode: "00",
        returnMessage: "Success.",
      }
    }

    const result = handleResponse(response)

    expect(result).toBe(true);
  })
})

