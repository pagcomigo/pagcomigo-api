const { creditCardSchema } = require('../schemas');

module.exports = (sequelize, DataTypes) => {
  const transactions = sequelize.define('transactions', {
    boleto: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    amount: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        min: 0.01
      }
    },
    installments: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        min: 1,
        max: 12
      }
    },
    transaction_gateway_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    creditCard: {
      type: DataTypes.VIRTUAL,
      validate: {
        isValid: function(creditCard) {
          const result = creditCardSchema.validate(creditCard, { abortEarly: false })
          return result;
        }
      }
    }
  }, {});
  transactions.associate = function(models) {};

  transactions.schemaValidate = function(transaction) {
    const validation = createTransactionSchema.validate(transaction, { abortEarly: false });

    return validation.error;
  }

  return transactions;
};