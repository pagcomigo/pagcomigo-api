const { createCustomerSchema } = require('../schemas');

module.exports = (sequelize, DataTypes) => {
  const customers = sequelize.define('customers', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false
    },
    document: {
      type: DataTypes.STRING,
      allowNull: false
    },
    documentType: {
      type: DataTypes.ENUM('CPF', 'CNPJ'),
      allowNull: false
    }
  }, {});
  customers.associate = function(models) {
    customers.hasMany(models.transactions, {
      foreignKey: {
        allowNull: false,
        name: 'customer_id'
      }
    })
  };

  customers.schemaValidate = function(customer) {
    const validation = createCustomerSchema.validate(customer, { abortEarly: false });

    return validation.error;
  }

  return customers;
};